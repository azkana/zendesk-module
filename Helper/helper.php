<?php

if(! function_exists('__getHelpCenterStateLabel')) {
    function __getHelpCenterStateLabel($state) {
        switch($state) {
            case 'enabled':
                $result = '<span class="label label-success">' . capitalize($state) . '</span>';
                break;
            case 'disabled':
                $result = '<span class="label label-danger">' . capitalize($state) . '</span>';
                break;
            case 'restricted':
                $result = '<span class="label label-warning">' . capitalize($state) . '</span>';
                break;
        }

        return $result;
    }
}