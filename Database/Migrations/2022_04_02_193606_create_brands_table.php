<?php

use App\Common\CustomBlueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Zendesk\Entities\ZendeskModel;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    protected $tablePrefix = ZendeskModel::DB_TABLE_PREFIX;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = DB::connection()->getSchemaBuilder();

        $schema->blueprintResolver(function ($table, $callback) {
            return new CustomBlueprint($table, $callback);
        });
        
        $schema->create($this->tablePrefix . 'brands', function (CustomBlueprint $table) {
            $table->uuid('id');
            $table->string('brand_id', 20)->unique();
            $table->string('name', 100);
            $table->string('brand_url', 200);
            $table->string('url', 200);
            $table->string('subdomain', 100);
            $table->boolean('has_help_center')->default(false);
            $table->string('help_center_state', 20);
            $table->boolean('active')->default(true);
            $table->boolean('default')->default(false);
            $table->text('logo')->nullable();
            $table->text('ticket_form_ids')->nullable();
            $table->string('host_mapping')->nullable();
            $table->timestamp('created_date')->nullable();
            $table->timestamp('updated_date')->nullable();
            $table->boolean('deleted')->default(false);
            $table->commonFields();
            $table->uuid('sync_by');
            $table->timestamp('sync_at')->nullable();

            $table->primary('id');
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablePrefix . 'brands');
    }
}
