<?php

namespace Modules\Zendesk\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Zendesk\Database\Seeders\CoreTableSeeder;

class ZendeskDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CoreTableSeeder::class);
    }
}
