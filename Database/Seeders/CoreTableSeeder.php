<?php

namespace Modules\Zendesk\Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Master\Role;
use Illuminate\Database\Seeder;
use App\Models\Master\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class CoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            'zendesk',
            'zendesk-dashboard-read', 'zendesk-ticket',
            'zendesk-ticket-read', 'zendesk-ticket-create', 'zendesk-ticket-edit', 'zendesk-ticket-export',
            'zendesk-brand-read', 'zendesk-brand-sync',
        ];

        foreach($permissions as $row) {
            $permission = Permission::where('name', $row)->first();
            if($permission) {
                $this->command->info('Permission ' .$row. ' already exists.');
            } else {
                Permission::create([
                    'name' => $row
                ]);
                $this->command->info('Permission ' .$row. ' created successfully.');
            }
        }

        $roles = ['zen-agent', 'zen-manager'];

        foreach($roles as $row) {
            $role = Role::where('name', $row)->first();
            if($role) {
                $this->command->info('Role ' .$row. ' already exists.');
                $thisRole = $role;
            } else {
                $newRole = Role::create([
                    'name' => $row
                ]);
                $this->command->info('Role ' .$row. ' created successfully.');
                $thisRole = $newRole;
            }

            switch($row) {
                case 'zen-manager':
                    $thisRole->givePermissionTo($permissions);
                    $thisRole->givePermissionTo([
                        'dashboard', 'settings', 'modules', 'user-profile',
                    ]);
                    break;
                case 'zen-agent':
                    $thisRole->givePermissionTo([
                        'zendesk-ticket-read', 'zendesk-ticket-export', 
                        // 'zendesk-brand-read', 'zendesk-brand-sync',
                    ]);
                    $thisRole->givePermissionTo([
                        'dashboard', 'settings', 'modules', 'user-profile',
                    ]);
                    break;
            }
            $this->command->info('Role ' . $row .' syncronized with permissions successfully.');
        }

        /**
         * Create default user
         */

        $users = [
            ['name' => 'Zendesk Agent', 'email' => 'zenagent@demo.com', 'role' => 'zen-agent'],
            ['name' => 'Zendesk Manager', 'email' => 'zenmanager@demo.com', 'role' => 'zen-manager'],
        ];

        foreach($users as $row) {
            
            $user = User::where('email', $row['email'])->first();
            
            if($user) {
                
                $this->command->info('User ' . $row['name'] . ' already exists.');
            
            } else {
                
                $newUser = User::create([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'password' => Hash::make('secret'),
                    'email_verified_at' => Carbon::now(),
                    'is_active' => true,
                    'created_by' => config('setting.db_sysadmin_uuid')
                ]);

                $newUser->assignRole($row['role']);

                $this->command->info('User ' . $row['name'] . ' with role ' . $row['role'] . ' created successfully.');
            
            }
            
            $this->command->info(' All User granted successfully.');
        }

    }
}
