<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('zendesk')->group(function() {
//     Route::get('/', 'ZendeskController@index');
// });

Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('zendesk')->group(function() {
            Route::name('zendesk.')->group(function() {

                Route::get('dashboard', 'ZendeskController@index')->name('dashboard');

                Route::prefix('ticket')->group(function() {
                    Route::get('/', 'Admin\TicketController@index')->name('ticket.index');
                    Route::get('data', 'Admin\TicketController@indexData')->name('ticket.index.data');
                    Route::get('search', 'Admin\TicketController@search')->name('ticket.search');
                    Route::get('export/excel', 'Admin\TicketController@exportToExcel')->name('ticket.export.excel');

                    Route::get('search/test', 'Admin\TicketController@searchTest')->name('ticket.search.test');
                });

                Route::prefix('brand')->group(function() {
                    Route::get('/', 'Admin\BrandController@index')->name('brand.index');
                    Route::get('data', 'Admin\BrandController@indexData')->name('brand.index.data');
                    Route::get('sync', 'Admin\BrandController@sync')->name('brand.sync');
                });

            });
        });
    });
});