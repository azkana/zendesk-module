@can('zendesk')
<li class="treeview @if($adminActiveMenu == 'zendesk') active @endif">
    <a href="#">
        <i class="fa fa-ticket"></i>
        <span>Zendesk</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <span class="label label-danger pull-right">
                New
            </span>
        </span>
    </a>
    <ul class="treeview-menu">
        {{-- @can('zendesk-dashboard-read')
        <li class="@if($adminActiveSubMenu == 'dashboard') active @endif">
            <a href="{!! route('zendesk.dashboard') !!}">
                <i class="fa fa-circle-o"></i> Dashboard
                <span class="pull-right-container"></span>
            </a>
        </li>
        @endcan --}}
        @can('zendesk-ticket')
            <li class="@if($adminActiveSubMenu == 'ticket') active @endif">
                <a href="#"><i class="fa fa-circle-o"></i> Tickets
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('zendesk-ticket-read')
                        <li class="@if($adminActiveSubMenu == 'ticket' && $urlSegment4 == '') active @endif">
                            <a href="{!! route('zendesk.ticket.index') !!}">
                                <i class="fa fa-circle-o"></i> Tickets List
                                <span class="pull-right-container"></span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
            @can('zendesk-brand-read')
                <li class="@if($adminActiveSubMenu == 'brand' && $urlSegment4 == '') active @endif">
                    <a href="{!! route('zendesk.brand.index') !!}">
                        <i class="fa fa-circle-o"></i> Brands
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endcan
        @endcan
        
    </ul>
</li>
@endcan