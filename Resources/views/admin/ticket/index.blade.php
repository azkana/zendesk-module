@extends('zendesk::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        @can('zendesk-ticket-export')
                            <a class="btn btn-sm btn-success" href="{!! route('zendesk.ticket.export.excel') !!}">
                                <i class="fa fa-file-excel-o"></i> Export to Excel
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="start_date" class="small">Brand</label>
                                        <div class="input-group date">
                                            {!! Form::select('brand', $brand, 'kedaireka', [ 'class' => 'form-control input-sm', 'id' => 'brand']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="button" class="small"></label>
                                        <div class="input-group">
                                            <button class="btn btn-sm btn-success" style="margin-top: 4.5px" id="btn_search">
                                                <i class="fa fa-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="grid-tickets" class="table table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 2%; vertical-align: middle">#</th>
                                        <th class="text-center" style="width: 2%; vertical-align: middle">Ticket ID</th>
                                        <th class="text-center" style="width: 15%; vertical-align: middle">Subject</th>
                                        <th class="text-center" style="vertical-align: middle">Description</th>
                                        <th class="text-center" style="width: 10%; vertical-align: middle">Requester Email</th>
                                        <th class="text-center" style="width: 10%; vertical-align: middle">Channel</th>
                                        <th class="text-center" style="width: 10%; vertical-align: middle">Status</th>
                                        <th class="text-center" style="width: 5%; vertical-align: middle">Created</th>
                                        <th class="text-center" style="width: 5%; vertical-align: middle">Updated</th>
                                        <th class="text-center" style="width: 5%; vertical-align: middle">
                                            <i class="fa fa-navicon"></i>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var brand = $('#brand');
            var btnSearch = $('#btn_search');

            var table = $('#grid-tickets').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paging:true,
                retrieve: true,
                deferRender: true,
                keys: false,
                stateSave: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                buttons: [
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                ],
                ajax: { 
                    url: "{!! route('zendesk.ticket.index.data') !!}",
                    data: function(d) {
                        d.brand = brand.val()
                    },
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'id', name: 'id', className: "small", orderable: false},
                    {data: 'subject', name: 'subject', className: "small", orderable: false},
                    {data: 'description', name: 'description', className: "small", orderable: false},
                    {data: 'requester_email', name: 'requester_email', className: "small", orderable: false},
                    {data: 'channel', name: 'channel', className: "small", orderable: false},
                    {data: 'status', name: 'status', className: "small", orderable: false},
                    {data: 'created_at', name: 'created_at', className: "small text-right"},
                    {data: 'updated_at', name: 'updated_at', className: "small text-right"},
                    {data: 'action', name: 'action', className: "small text-center", orderable: false, searchable: false},
                ],
                columnDefs: [
                    {targets: 7, render: $.fn.dataTable.render.moment('YYYY-MM-DDTHH:mm:ssZ', 'DD-MM-YYYY HH:mm:ss' )},
                    {targets: 8, render: $.fn.dataTable.render.moment('YYYY-MM-DDTHH:mm:ssZ', 'DD-MM-YYYY HH:mm:ss' )},
                ],
                order: [
                    [8, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });

            btnSearch.on('click', function(e) {
                e.preventDefault();
                getData();
            });

            function getData() {
                $.ajax({
                    url: "{!! route('zendesk.ticket.index.data') !!}",
                    type: "GET",
                    data: {
                        brand: brand.val(),
                    },
                    success: function(data) {
                        table.ajax.reload();
                    }
                });
            }
        });
    </script>
@endsection