<table>
    <tr>
        <th>Created Date</th>
        <th>Ticket ID</th>
        <th>Ticket Status</th>
        <th>Kedaireka - Posisi</th>
        <th>Requester Name</th>
        <th>Requester Email</th>
        <th>Subject</th>
        <th>Description</th>
        <th>Recipient</th>
        <th>Channel</th>
    </tr>
    @foreach ($data as $items)
    <tr>
        <td>{!! dateFormatYmd($items->created_at) !!}</td>
        <td>{!! $items->id !!}</td>
        <td>{!! $items->status !!}</td>
        <td></td>
        <td></td>
        <td></td>
        <td>{!! htmlentities($items->subject) !!}</td>
        <td>{!! htmlentities($items->description) !!}</td>
        <td>{!! $items->recipient !!}</td>
        <td>{!! $items->via->channel !!}</td>
    </tr>
    @endforeach
</table>