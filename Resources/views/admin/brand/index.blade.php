@extends('zendesk::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        @can('zendesk-brand-sync')
                            <a class="btn btn-sm btn-info" href="{!! route('zendesk.brand.sync') !!}">
                                <i class="fa fa-refresh"></i> Sync
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="grid-tickets" class="table table-bordered table-striped table-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 2%; vertical-align: middle">#</th>
                                        <th class="text-center" style="width: 2%; vertical-align: middle">Brand ID</th>
                                        <th class="text-center" style="vertical-align: middle">Brand Name</th>
                                        <th class="text-center" style="width: 12%; vertical-align: middle">Subdomain</th>
                                        <th class="text-center" style="width: 10%; vertical-align: middle">Ticket<br>Form ID</th>
                                        <th class="text-center" style="width: 7%; vertical-align: middle">Help Center</th>
                                        <th class="text-center" style="width: 7%; vertical-align: middle">Status</th>
                                        <th class="text-center" style="width: 5%; vertical-align: middle">Created</th>
                                        <th class="text-center" style="width: 5%; vertical-align: middle">Updated</th>
                                        {{-- <th class="text-center" style="width: 5%; vertical-align: middle">
                                            <i class="fa fa-navicon"></i>
                                        </th> --}}
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#grid-tickets').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                buttons: [
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                ],
                ajax: { 
                    url: "{!! route('zendesk.brand.index.data') !!}",
                    pages: 100,
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small text-center", orderable: false, searchable: false},
                    {data: 'brand_id', name: 'brand_id', className: "small text-center", orderable: false},
                    {data: 'name', name: 'name', className: "small", orderable: false},
                    {data: 'subdomain', name: 'subdomain', className: "small", orderable: false},
                    {data: 'ticket_form_ids', name: 'ticket_form_ids', className: "small", orderable: false},
                    {data: 'help_center', name: 'help_center', className: "small", orderable: false},
                    {data: 'active', name: 'active', className: "small", orderable: false},
                    {data: 'created_date', name: 'created_date', className: "small text-right"},
                    {data: 'updated_date', name: 'updated_date', className: "small text-right"},
                    // {data: 'action', name: 'action', className: "small text-center", orderable: false, searchable: false},
                ],
                columnDefs: [
                    {targets: 7, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                    {targets: 8, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                ],
                // order: [
                    // [8, 'desc']
                // ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });
        });
    </script>
@endsection