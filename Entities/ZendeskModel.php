<?php

namespace Modules\Zendesk\Entities;

use App\Models\BaseModel;

class ZendeskModel extends BaseModel
{
    public const DB_TABLE_PREFIX = 'zend_';
}
