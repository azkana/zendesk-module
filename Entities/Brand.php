<?php

namespace Modules\Zendesk\Entities;

use App\Traits\Uuid as TraitsUuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Brand extends ZendeskModel
{
    use HasFactory, TraitsUuid, SoftDeletes;

    protected $table = ZendeskModel::DB_TABLE_PREFIX . 'brands';

    protected $fillable = [
        'brand_id',
        'name',
        'brand_url',
        'url',
        'subdomain',
        'has_help_center',
        'help_center_state',
        'active',
        'default',
        'logo',
        'ticket_form_ids',
        'host_mapping',
        'created_date',
        'updated_date',
        'deleted',
        'sync_by',
        'sync_at'
    ];

    public static function _getList()
    {
        return self::pluck('name', 'subdomain');
    }

}
