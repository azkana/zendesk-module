<?php

namespace Modules\Zendesk\Exports;

use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class TicketExportToExcel extends DefaultValueBinder implements FromView, ShouldAutoSize, WithCustomValueBinder, WithColumnFormatting
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    } 

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        if(is_string($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {
        return view('zendesk::admin.exports.ticket-excel', [
            'data' => $this->data
        ]);
    }

    public function columnFormats(): array
    {
        return [
            // 'A' => NumberFormat::FORMAT_NUMBER,
            // 'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'I' => NumberFormat::FORMAT_TEXT,
            // 'N' => NumberFormat::FORMAT_TEXT,
            // 'S' => NumberFormat::FORMAT_TEXT,
            // 'X' => NumberFormat::FORMAT_TEXT,
            // 'Y' => NumberFormat::FORMAT_TEXT,
            // 'AD' => NumberFormat::FORMAT_TEXT,
            // 'AE' => NumberFormat::FORMAT_TEXT,
            // 'AJ' => NumberFormat::FORMAT_TEXT,
            // 'AK' => NumberFormat::FORMAT_TEXT,
            // 'AM' => NumberFormat::FORMAT_TEXT,
            // 'AN' => NumberFormat::FORMAT_TEXT,
            // 'AQ' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'AU' => NumberFormat::FORMAT_TEXT,
            // 'AY' => NumberFormat::FORMAT_NUMBER,
            // 'BD' => NumberFormat::FORMAT_NUMBER,
        ];
    }

}
