<?php

namespace Modules\Zendesk\Http\Controllers\Admin;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Zendesk\Entities\Brand;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Contracts\Support\Renderable;

class BrandController extends Controller
{
    protected $pageTitle;
    protected $client;
    protected $username, $token, $subdomain;

    public function __construct()
    {
        $this->pageTitle = 'Brands';
        $this->subdomain = config('zendesk-laravel.subdomain');
        $this->username = config('zendesk-laravel.username');
        $this->token = config('zendesk-laravel.token');
        $this->client   = new Client([
            'verify' => false
        ]);
    }
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('zendesk::admin.brand.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {

            $data = new Brand();
            $data = $data->orderBy('default', 'desc')
                ->orderBy('updated_date', 'desc')
                ->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function($row) {
                    $logo = !is_null($row->logo) ? '<img src="' . $row->logo . '" style="max-height: 30px; max-width: 40px" /> &nbsp;' : null;
                    $default = $row->default == 1 ? ' <span class="label bg-maroon-active"><i class="glyphicon glyphicon-pushpin"></i> default</span>' : null;
                    $name = $logo . $row->name . $default;
                    return $name;
                })
                ->addColumn('subdomain', function($row) {
                    $subdomain =  $row->subdomain . '<a href="' . $row->brand_url . '" target="_blank" title="click for detail"> <i class="fa fa-external-link"></i></a>';
                    return $subdomain;
                })
                ->addColumn('help_center', function($row) {
                    return __getHelpCenterStateLabel($row->help_center_state);
                })
                ->addColumn('active', function($row) {
                    return __getStateLabel($row->active);
                })
                ->addColumn('action', function($row) {
                    return null;
                })
                ->rawColumns([
                    'action', 'help_center', 'active', 'subdomain', 'name'
                ])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function sync()
    {
        try {
            $url        = 'https://' . $this->subdomain . '.zendesk.com/api/v2/brands.json';
            $request    = $this->client->get($url, [
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode($this->username . '/token:' . $this->token),
                    'Content-Type' => 'application/json'
                ],
            ]);
            $response   = $request->getBody()->getContents();
            $result = json_decode($response);

            if($result->count > 0) {

                foreach($result->brands as $row) {

                    $data['brand_id']   = $row->id;
                    $data['name']       = $row->name;
                    $data['brand_url']  = $row->brand_url;
                    $data['url']        = $row->url;
                    $data['subdomain']  = $row->subdomain;
                    $data['has_help_center']    = $row->has_help_center;
                    $data['help_center_state']  = $row->help_center_state;
                    $data['active']     = $row->active;
                    $data['default']    = $row->default;
                    $data['ticket_form_ids']    = implode(', ', $row->ticket_form_ids);
                    $data['created_date']       = dateFormatYmdHis($row->created_at);
                    $data['updated_date']       = dateFormatYmdHis($row->updated_at);
                    $data['deleted']            = $row->is_deleted;
                    $data['host_mapping']       = $row->host_mapping;

                    $data['logo']       = !is_null($row->logo) ? $row->logo->content_url : null;
                    $data['sync_by']    = Auth::user()->id;
                    $data['sync_at']    = Carbon::now();

                    $checkBrand = Brand::where('brand_id', $row->id)->first();
                    if($checkBrand) {
                        if($checkBrand->updated_at == dateFormatYmdHis($row->updated_at)) {
                            $checkBrand->update($data);
                        }
                    } else {
                        Brand::create($data);
                    }
                }
            }

            return redirect()->route('zendesk.brand.index')
                ->with('success', trans('message.success_save'));
        } catch(Exception $e) {
            return redirect(route('zendesk.brand.index'))
				->with('error', trans('message.error_save'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('zendesk::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('zendesk::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
