<?php

namespace Modules\Zendesk\Http\Controllers\Admin;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Zendesk\Entities\Brand;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Contracts\Support\Renderable;
use Modules\Zendesk\Exports\TicketExportToExcel;

class TicketController extends Controller
{

    protected $pageTitle;
    protected $client;
    protected $username, $token, $subdomain;

    public function __construct()
    {
        $this->pageTitle = 'Ticket';
        $this->subdomain = config('zendesk-laravel.subdomain');
        $this->username = config('zendesk-laravel.username');
        $this->token = config('zendesk-laravel.token');
        $this->client   = new Client([
            'verify' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['brand']        = Brand::_getList();
        return view('zendesk::admin.ticket.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {

            $data = $this->search($request)->results;

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('description', function($row) {
                    $str = '------------------';
                    $desc = $row->description;
                    if(str_contains($desc, $str)) {
                        $desc = strstr($desc, '------------------', true);
                    }
                    return $desc;
                })
                ->addColumn('requester_email', function($row) {
                    // $requester = \__getUserDetail($row->requester_id)->email;
                    $requester = $row->requester_id['email'];
                    return $requester;
                })
                ->addColumn('channel', function($row) {
                    return $row->via->channel;
                })
                ->addColumn('action', function($row) {
                    return null;
                })
                ->rawColumns([
                    'action', 'channel', 'requester_email'
                ])
                ->make(true);
        }
    }

    public function search($req)
    {
        $query = 'type:ticket ';
        if(isset($req->brand)) {
            $query = $query . ' brand:' . $req->brand;
        }
        $url        = 'https://' . $this->subdomain . '.zendesk.com/api/v2/search.json';
        $request    = $this->client->get($url, [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($this->username . '/token:' . $this->token),
                'Content-Type' => 'application/json'
            ],
            'query' => [
                'query' => $query,
                'sort_by' => 'created_at',
                'sort_order' => 'desc'
            ]
        ]);
        $response   = $request->getBody()->getContents();
        $result = json_decode($response);
        return $result;
    }

    public function exportToExcel()
    {
        
        $data = $this->search()->results;

        return Excel::download(new TicketExportToExcel($data), 'ticket_'.date('Y-m-d').'.xlsx');
        
    }

    public function searchTest()
    {
        $url        = 'https://' . $this->subdomain . '.zendesk.com/api/v2/search.json';
        $request    = $this->client->get($url, [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($this->username . '/token:' . $this->token),
                'Content-Type' => 'application/json'
            ],
            'query' => [
                'query' => 'type:ticket created>='. date('Y-m-d') .' brand:Kedaireka',
                'sort_by' => 'created_at',
                'sort_order' => 'desc',
                'include' => 'tickets(users)',
            ]
        ]);
        $response   = $request->getBody()->getContents();
        $result = json_decode($response);

        // foreach($result->results as $row) {
        //     // dd($row->via);
        //     foreach ($row->fields as $item) {
        //         dd($item->id);
        //     }
        // }
        dd($result->results);
        return $result;
    }

}
